package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        return null;
    }

    public Boolean isValid(int start, int end) {
        if (isInRange(start) && isInRange(end) && isStartNotBiggerThanEnd(start, end)) return true;
        else return false;
    }

    public Boolean isInRange(int number) {
        if (number >= 1 && number <= 1000) return true;
        else return false;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        if (start <= end) return true;
        else return false;
    }

    public String generateTable(int start, int end) {
        String table = "";
        for (int i = start; i <= end; i++) {
            table += generateLine(start, i);
            if (i != end) table += String.format("%n");
        }
        return table;
    }

    public String generateLine(int start, int row) {
        String line = "";
        for (int i = start; i <= row; i++) {
            line += generateSingleExpression(i, row);
            if (i != row) line += "  ";
        }
        return line;
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        return multiplicand + "*" + multiplier + "=" + multiplicand * multiplier;
    }
}
